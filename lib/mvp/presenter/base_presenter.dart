import 'package:meta/meta.dart';
import 'package:macaron/mvp/view.dart';
import 'package:macaron/mvp/presenter.dart';

abstract class BasePresenter<V extends View> implements Presenter<V> {
  BasePresenter()
      : _hasView = false,
        _hasInitialized = false;

  V? _view;

  bool _hasView;

  // TODO(): Consider adding lifecycle switchs.
  bool _hasInitialized;

  bool get hasView => _hasView;

  bool get hasInitialized => _hasInitialized;

  @override
  V get view {
    assert(hasView, 'the presenter has not attaching any views yet');
    return _view!;
  }

  @override
  @mustCallSuper
  void attachView(V view) {
    _hasView = true;
    _view = view;

    if (!hasInitialized) {
      _hasInitialized = true;
      initialize();
    }
  }

  @override
  @mustCallSuper
  void detachView() {
    _view = null;
    _hasView = false;
  }

  @mustCallSuper
  void initialize() {}

  @override
  @mustCallSuper
  void dispose() {}

  @override
  void loadData() {}

  @mustCallSuper
  void onViewReady() {}

  @mustCallSuper
  void onViewDestroy() {}
}
