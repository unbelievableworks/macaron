import 'package:macaron/mvp/view.dart';
import 'package:macaron/mvp/presenter.dart';
import 'package:macaron/mvp/service_resolver.dart';

mixin ServiceProviderForPresenterMixin<V extends View> on Presenter<V>
    implements ServiceResolver {
  @override
  T? resolveService<T>({bool listen = false}) {
    return view.resolveService<T>(listen: listen);
  }
}
