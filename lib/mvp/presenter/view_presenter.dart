import 'dart:collection';

import 'package:meta/meta.dart';
import 'package:macaron/mvp/presenter/base_presenter.dart';
import 'package:macaron/mvp/presenter/helpers/service_resolver.dart';
import 'package:macaron/mvp/view.dart';

typedef PostponedTask = void Function();

abstract class ViewPresenter<V extends View> extends BasePresenter<V>
    with ServiceProviderForPresenterMixin<V> {
  ViewPresenter()
      : _postponedTasks = ListQueue<PostponedTask>(),
        super();

  Queue<PostponedTask> _postponedTasks;

  @override
  @mustCallSuper
  void onViewReady() {
    super.onViewReady();

    _flushPostponedTasks();
  }

  void schedulePostpone(PostponedTask task) {
    if (hasView) {
      task();
      return;
    }

    _postponedTasks.addLast(task);
  }

  void _flushPostponedTasks() {
    while (_postponedTasks.isNotEmpty) {
      final task = _postponedTasks.removeFirst();
      task();
    }
  }
}
