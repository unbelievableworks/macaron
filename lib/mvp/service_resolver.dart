abstract class ServiceResolver {
  T? resolveService<T>({bool listen = false});
}
