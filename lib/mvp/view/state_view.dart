import 'package:flutter/material.dart';
import 'package:macaron/mvp/view/helpers/service_resolver.dart';
import 'package:macaron/mvp/view/helpers/update.dart';
import 'package:macaron/mvp/view.dart';
import 'package:macaron/mvp/presenter.dart';

abstract class StateView<S extends StatefulWidget, P extends Presenter>
    extends State<S>
    with ServiceResolverForStateMixin
    implements View, UpdateOwner {
  P? _presenter;

  late UpdateScheduler _updateScheduler;

  P get presenter {
    assert(_presenter != null);
    return _presenter!;
  }

  @override
  void initState() {
    super.initState();

    _updateScheduler = BatchUpdateScheduler(this);

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      _notifyPresenterOnViewReady();
      onReady();
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _resolveAndBindPresenter();
  }

  P resolvePresenter() => resolveService<P>()!;

  void _resolveAndBindPresenter() {
    final newPresenter = resolvePresenter();
    if (_presenter != newPresenter) {
      _presenter?.detachView();

      newPresenter.attachView(this);
      newPresenter.loadData();

      _presenter = newPresenter;
    }
  }

  void onReady() {}

  @override
  void setState(VoidCallback fn) {
    scheduleUpdate(fn);
  }

  @override
  void commitUpdate(void Function() update, void Function() effect) {
    super.setState(() {
      update();
    });
    effect();
  }

  void scheduleUpdate([SimpleUpdate? update]) {
    _updateScheduler
        .schduleUpdate(update != null ? liftUpdate(update) : noopUpdate);
  }

  void scheduleUpdateWithEffect([Update? update]) {
    _updateScheduler.schduleUpdate(update ?? noopUpdate);
  }

  @override
  void dispose() {
    _presenter?.onViewDestroy();
    _presenter?.detachView();
    super.dispose();
  }

  void _notifyPresenterOnViewReady() {
    presenter.onViewReady();
  }
}
