abstract class DataSource {
  void loadData();

  void refreshData();
}
