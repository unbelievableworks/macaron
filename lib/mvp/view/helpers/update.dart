import 'dart:async';
import 'dart:collection';

typedef UpdateEffect = void Function();

typedef Update = UpdateEffect Function();

typedef SimpleUpdate = void Function();

UpdateEffect noopUpdateEffect = () => null;

Update noopUpdate = () => noopUpdateEffect;

Update liftUpdate(SimpleUpdate update) {
  return () {
    update();
    return noopUpdateEffect;
  };
}

abstract class UpdateOwner {
  void commitUpdate(void Function() update, void Function() effect);
}

abstract class UpdateScheduler {
  void schduleUpdate(Update updater);
}

class BatchUpdateScheduler implements UpdateScheduler {
  BatchUpdateScheduler(this._owner)
      : _needsFlushForNextUpdate = true,
        _bufferedUpdates = ListQueue();

  final UpdateOwner _owner;

  bool _needsFlushForNextUpdate;

  Queue<Update> _bufferedUpdates;

  @override
  void schduleUpdate(Update update) {
    _enqueueBufferedUpdate(update);
    _flushUpdatesIfNeeds();
  }

  void _enqueueBufferedUpdate(Update update) {
    _bufferedUpdates.addLast(update);
  }

  Queue<Update> _swapBufferedUpdates() {
    final bufferedUpdates = ListQueue<Update>.from(_bufferedUpdates);

    _bufferedUpdates.clear();
    _needsFlushForNextUpdate = true;

    return bufferedUpdates;
  }

  void _flushUpdatesOnNextTick() {
    if (_bufferedUpdates.isEmpty) {
      return;
    }

    scheduleMicrotask(() {
      final updates = _swapBufferedUpdates();
      final effects = <UpdateEffect>[];

      void doUpdate() {
        while (updates.isNotEmpty) {
          final update = updates.removeFirst();
          effects.add(update());
        }
      }

      void doEffect() {
        effects.forEach((effect) {
          effect();
        });
      }

      _owner.commitUpdate(doUpdate, doEffect);
    });
  }

  void _flushUpdatesIfNeeds() {
    if (_needsFlushForNextUpdate) {
      _needsFlushForNextUpdate = false;
      _flushUpdatesOnNextTick();
    }
  }
}
