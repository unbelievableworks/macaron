import 'package:fluro/fluro.dart';

abstract class UseRouter {
  FluroRouter get router;
}