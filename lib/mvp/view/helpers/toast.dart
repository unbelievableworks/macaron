enum ToastDuration { long, short }

abstract class UseToast {
  void showToast(String message, ToastDuration duration);
}