import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:macaron/mvp/service_resolver.dart';

mixin ServiceResolverForStateMixin<S extends StatefulWidget> on State<S>
    implements ServiceResolver {
  @override
  T? resolveService<T>({bool listen = false}) {
    return Provider.of<T>(context, listen: listen);
  }
}
