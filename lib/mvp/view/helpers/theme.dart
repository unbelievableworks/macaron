import 'package:flutter/material.dart';

abstract class UseTheme {
  ThemeData get theme;
}

