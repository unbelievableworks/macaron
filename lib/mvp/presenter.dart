import 'package:macaron/mvp/service_resolver.dart';
import 'package:macaron/mvp/view.dart';
import 'package:macaron/utils/disposable.dart';

enum PresenterLifecycleState {
  attached,
  ready,
  detached,
}

abstract class Presenter<V extends View>
    implements ServiceResolver, Disposable {
  V get view;

  void attachView(V view);

  void detachView();

  void loadData();

  void onViewReady();

  void onViewDestroy();
}
