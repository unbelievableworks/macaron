int clampRowsPerPage(int expectedRows, int totalRows) {
  if (totalRows == 0) {
    return 1;
  }

  if (totalRows <= expectedRows) {
    return totalRows;
  }

  return expectedRows;
}
