abstract class Field {}

abstract class BaseField implements Field {
  const BaseField();
}

class TextField extends BaseField {
  const TextField() : super();
}

class NumberField extends BaseField {
  const NumberField() : super();
}

class IntegerField extends BaseField {
  const IntegerField() : super();
}

class DecimalField extends BaseField {
  const DecimalField() : super();
}

class DateTimeField extends BaseField {
  const DateTimeField() : super();
}

class DateField extends BaseField {
  const DateField() : super();
}

class TimeField extends BaseField {
  const TimeField() : super();
}

class TimestampField extends BaseField {
  const TimestampField() : super();
}

class EnumerationField extends BaseField {
  const EnumerationField() : super();
}

class NullField extends BaseField {
  const NullField() : super();
}
