abstract class DataType {}

class Number implements DataType {}

class Integer implements Number {}

class Decimal implements Number {}

class Text implements DataType {}

class Date implements DataType {}

class Time implements DataType {}

class DateTime implements DataType {}

class Timestamp implements DataType {}

class Boolean implements DataType {}

class Enumeration implements DataType {}