typedef ModelSeederDelegateFunc<T> = T Function(int index, T Function() seeder);

abstract class ModelSeeder<T> {
  T seed();
}

abstract class BaseModelSeeder<T> implements ModelSeeder<T> {
  List<T> batchSeed(int count, [ModelSeederDelegateFunc<T>? delegate]) {
    return List.generate(count,
        (index) => delegate != null ? delegate(index, () => seed()) : seed());
  }
}
