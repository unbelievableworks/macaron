import 'package:flutter/material.dart';
import 'package:macaron/mvp/view/helpers/theme.dart';

mixin UseThemeMixin<T extends StatefulWidget> on State<T> implements UseTheme {
  @override
  ThemeData get theme => Theme.of(context);
}
