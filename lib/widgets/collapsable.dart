import 'package:flutter/widgets.dart';

abstract class Collapsable {
  void collapse();

  void expand();

  void triggerCollapse();
}

mixin UseCollapsableMixin<T extends StatefulWidget> on State<T>
    implements Collapsable {
  late bool _collapsed;

  bool get allowCollapse => true;

  bool get initialIsCollapsed => false;

  bool get isCollapsed => allowCollapse ? _collapsed : initialIsCollapsed;

  bool get isExpanded => !isCollapsed;

  @override
  @mustCallSuper
  void initState() {
    super.initState();

    _collapsed = initialIsCollapsed;
  }

  void collapse() {
    setState(() {
      _collapsed = true;
    });
  }

  void expand() {
    setState(() {
      _collapsed = false;
    });
  }

  void triggerCollapse() {
    if (_collapsed) {
      expand();
      return;
    }

    collapse();
  }
}

class CollapsableScope extends InheritedWidget {
  CollapsableScope({
    Key? key,
    required this.allowCollapse,
    required this.isCollapsed,
    required Widget child,
  }) : super(key: key, child: child);

  final bool allowCollapse;

  final bool isCollapsed;

  bool get isExpanded => !isCollapsed;

  static CollapsableScope? of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<CollapsableScope>();

  @override
  bool updateShouldNotify(CollapsableScope oldWidget) =>
      allowCollapse != oldWidget.allowCollapse ||
      isCollapsed != oldWidget.isCollapsed;
}
