import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:macaron/mvp/view/helpers/toast.dart';

mixin UseToastMixin<T extends StatefulWidget> on State<T> implements UseToast {
  @override
  void showToast(String message, ToastDuration duration) {
    Toast? length;

    switch (duration) {
      case ToastDuration.long:
        length = Toast.LENGTH_LONG;
        break;
      case ToastDuration.short:
        length = Toast.LENGTH_SHORT;
        break;
      default:
        assert(false, 'unsupported toast duration \'$duration\'');
    }

    Fluttertoast.showToast(msg: message, toastLength: length);
  }
}
