import 'package:provider/provider.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:macaron/mvp/view/helpers/router.dart';

mixin UseRouterMixin<T extends StatefulWidget> on State<T>
    implements UseRouter {
  @override
  FluroRouter get router => context.read<FluroRouter>();
}
