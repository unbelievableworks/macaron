import 'package:fluro/fluro.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:macaron/mvp/presenter.dart';
import 'package:nested/nested.dart';

class AppProvider extends StatelessWidget {
  final FluroRouter router;
  final Widget child;

  const AppProvider({
    Key? key,
    required this.router,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<FluroRouter>.value(value: router),
      ],
      child: child,
    );
  }
}

class PresenterProvider<P extends Presenter> extends Provider<P> {
  PresenterProvider({
    Key? key,
    required P Function() presenter,
    Widget? child,
  }) : super(
          key: key,
          create: (_) => presenter(),
          dispose: (_, presenter) => presenter.dispose(),
          child: child,
        );
}
