abstract class Optional<T> {
  const Optional._internal(this._hasValue, this._value);

  final bool _hasValue;

  final T? _value;

  bool get hasValue => _hasValue;

  T? get value => _value;

  factory Optional.of(T value) {
    return Value(value);
  }

  factory Optional.ofNullable(T? value) {
    if (value == null) {
      return Null();
    }

    return Value(value);
  }

  T orElse(T Function() getter);

  Optional<R> map<R>(R? Function(T value) mapper);
}

class Value<T> extends Optional<T> {
  const Value(T value) : super._internal(true, value);

  @override
  T orElse(T Function() getter) {
    return value!;
  }

  @override
  Optional<R> map<R>(R? Function(T value) mapper) {
    return Optional.ofNullable(mapper(value!));
  }
}

class Null<T> extends Optional<T> {
  const Null() : super._internal(false, null);

  @override
  T orElse(T Function() getter) {
    return getter();
  }

  @override
  Optional<R> map<R>(R? Function(T value) mapper) {
    return Null();
  }
}
