abstract class EventHandler<E> {
  void handle(E event);
}

class _DirectEventHandler<E> implements EventHandler<E> {
  _DirectEventHandler({required this.handleCallback});

  void Function(E) handleCallback;

  @override
  void handle(E event) {
    handleCallback(event);
  }
}

abstract class AroundEventHandler<E> {
  void beforeHandle(E event);

  void handle(E event);

  void afterHandle(E event);
}

class _AroundEventHandlerAdapter<E> implements EventHandler<E> {
  _AroundEventHandlerAdapter(this.handler);

  AroundEventHandler<E> handler;

  @override
  void handle(E event) {
    handler.beforeHandle(event);
    handler.handle(event);
    handler.afterHandle(event);
  }
}

class _AroundEventHandler<E> implements AroundEventHandler<E> {
  _AroundEventHandler(
      {this.beforeHandleCallback,
      required this.handleCallback,
      this.afterHandleCallback});

  final Function(E)? beforeHandleCallback;

  final Function(E) handleCallback;

  final Function(E)? afterHandleCallback;

  @override
  void beforeHandle(E event) {
    beforeHandleCallback?.call(event);
  }

  @override
  void handle(E event) {
    handleCallback.call(event);
  }

  @override
  void afterHandle(E event) {
    afterHandleCallback?.call(event);
  }
}

class EventHandlerFactory {
  static EventHandler createDirectEventHandler<E>({
    required void Function(E) handleCallback,
  }) {
    return _DirectEventHandler<E>(
      handleCallback: handleCallback,
    );
  }

  static EventHandler createAroundEventHandler<E>({
    void Function(E)? beforeHandleCallback,
    required void Function(E) handleCallback,
    void Function(E)? afterHandleCallback,
  }) {
    return _AroundEventHandlerAdapter<E>(_AroundEventHandler<E>(
      beforeHandleCallback: beforeHandleCallback,
      handleCallback: handleCallback,
      afterHandleCallback: afterHandleCallback,
    ));
  }
}
