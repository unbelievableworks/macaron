import 'package:macaron/event/event_handler.dart';

class EventChannel<E> {
  EventChannel() : _handlers = {};

  final Set<EventHandler> _handlers;

  void publish(E event) {
    _handlers.forEach(((handler) {
      handler.handle(event);
    }));
  }

  void Function() subscribe(EventHandler handler) {
    _handlers.add(handler);

    return () => _handlers.remove(handler);
  }
}
