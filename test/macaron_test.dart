import 'package:flutter_test/flutter_test.dart';

import 'package:macaron/macaron.dart';

void main() {
  test('adds one to input values', () {
    expect(1, 1);
  });
}
